import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BgSliderService } from '../slider/slider.service';

@Component({
  selector: 'bg-slider',
  template: 
  `<input 
    type="range" 
    [attr.max]="maxVal"
    [attr.min]="minVal"
    [attr.value]="currentVal" (input)="OnSliderChange($event)" />  
  `,
  styleUrls: ['./slider.component.less']
})
export class SliderComponent implements OnInit {

  @Input('min') minVal: number;
  @Input('max') maxVal: number;
  @Input('value') currentVal: number;
  @Output('OnChange') Change = new EventEmitter<number>();
  constructor(private bgSliderService: BgSliderService) { }

  ngOnInit() {
  }

  OnSliderChange(evt) {
    //this.Change.emit(+evt.target.value);
    this.bgSliderService.OnSelected(+evt.target.value);
  }

}
