import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';

@Injectable()
export class BgSliderService {
    private bgSliderSource = new Rx.Subject<number>();
    bgSlider$ = this.bgSliderSource.asObservable();

    getBgSlider$() {
        return this.bgSlider$;
    }

    OnSelected(val: number) {
        //console.log(`slider service ${val}`);
        this.bgSliderSource.next(val);
    }
}