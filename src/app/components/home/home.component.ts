import { Component, OnInit, OnDestroy } from '@angular/core';
import { BgSliderService } from '../slider/slider.service';
import * as Rx from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  sliderSub: Rx.Subscription;
  currentSliderVal: number = 0;
  constructor(private bgSliderService: BgSliderService) { }
  obj = [{name: 'Hello 1'}, {name: 'Hello 2'}];
  ngOnInit() {
    this.sliderSub = this.bgSliderService.getBgSlider$().subscribe(value => {
      console.log(`from subscribe ${value}`);
      this.currentSliderVal = value;
    });

    // var sampleText = '';
    // for(var i = 5; i < 1089; i++) {
    //   sampleText = sampleText + `${i}px 0 0 -5px @UpperBackGroundColor, `;
    // }

    // console.log(sampleText);
  }

  ngOnDestroy() {
    if(this.sliderSub)
      this.sliderSub.unsubscribe();
  }

  SliderChange(value: number) {
    console.log(`current selected value is ${value}`);
  }

  check(): void {
    debugger;
  }

}
