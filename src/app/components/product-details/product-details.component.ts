import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    debugger;
    let movie1 = {
      name: 'Star Wars',
      episode: 7
    };

    let movie2 = Object.assign({}, movie1);
    movie2.episode = 8;
    console.log(movie1.episode); // writes 7
    console.log(movie2.episode); // writes 8
  }

}
