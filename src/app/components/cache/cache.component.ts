import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../data.service';
import * as Rx from 'rxjs';

@Component({
    selector: 'cache',
    templateUrl: './cache.component.html'
})
export class CacheComponent implements OnInit, OnDestroy {

    constructor(private dataService: DataService) {

    }

    users: any = [];
    columnsArr: any = [];
    table: any = [];
    postData: any = [];
    usersSub: any;
    postsSub: any;
    userid: number;
    dataObs$ = new Rx.BehaviorSubject<any>(0);
    ngOnInit() {
        this.usersSub = this.dataService.getUsers().subscribe(res => {
            this.users = res;
        });
    }

    ngOnDestroy() {
        if (this.usersSub)
            this.usersSub.unsubscribe();

        if (this.postsSub)
            this.postsSub.unsubscribe();
    }

    onUserChanged(userobjStr: any) {
        if (userobjStr == '') {
            this.table = [];
            return;
        }
        let userObj = JSON.parse(userobjStr);
        this.userid = userObj.id;
        this.postsSub = this.dataService.getPostsById(userObj.id).subscribe(res => {
            this.postData = res;
            this.table = res;
            this.columnsArr = [];
            for (let i = 0; i < this.table.length; i++) {
                let columns = Object.keys(this.table[i]);
                for (let j = 0; j < columns.length; j++) {
                    let _obj = {};
                    _obj['name'] = columns[j];
                    _obj['is_desc'] = false;

                    this.columnsArr.push(_obj);
                }
                break;
            }

            this.dataObs$.next(res);
        });
    }

    onPostChanged(val: number) {
        let posts = Object.assign([], this.dataObs$.value);
        let SelectedPost;
        this.table = [];
        if (val != 0) {
            SelectedPost = posts.filter(x => x.id == val);
            this.table = SelectedPost;
        } else {
            this.table = posts;
        }
    }

    sort(column: string) {
        let sortInfo = this.columnsArr.find(x => x.name == column);
        this.table = this.table.sort(function (a, b) {
            if (sortInfo.is_desc) {
                if (a[column] < b[column])
                    return -1;
                else if (a[column] > b[column])
                    return 1;
            } else {
                if (a[column] < b[column])
                    return 1;
                else if (a[column] > b[column])
                    return -1;
            }

            return 0;
        });

        sortInfo.is_desc = !sortInfo.is_desc;
    }

    checkData() {
        let _table = this.table;
        debugger;
    }

    resetData() {
        let _table = Object.assign([], this.dataObs$.value);
        let _temp = JSON.parse(JSON.stringify(this.dataObs$.value))
        debugger;
    }

}