import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Subject, ReplaySubject } from '../../node_modules/rxjs';


@Injectable()
export class DataService {

    dataObs$ = new BehaviorSubject<any>(0);
    constructor(private http: HttpClient) {

    }
    getUsers() {
        return this.http.get('https://jsonplaceholder.typicode.com/users');
    }

    getPostsById(id: number) {
        return this.http.get(`https://jsonplaceholder.typicode.com/posts?userId=${id}`);
    }
}