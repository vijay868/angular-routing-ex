import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { OverviewComponent } from './components/overview/overview.component';
import { SpecsComponent } from './components/specs/specs.component';
import { SliderComponent } from './components/slider/slider.component';
import { TouchSpinComponent } from './components/touch-spin/touch-spin.component';

import { BgSliderService } from './components/slider/slider.service';
import { ViewChildrenComponent } from './components/view-children/view-children.component';
import { DataService } from './data.service';
import { CacheComponent } from './components/cache/cache.component';

import { CustomMaxDirective } from './directives/custom-max-validator.directive';
import { CustomMinDirective } from './directives/custom-min-validator.directive';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'product-details/:id', component: ProductDetailsComponent,
    children: [
      { path: 'overview', component: OverviewComponent },
      { path: 'specs', component: SpecsComponent },
    ]
  },
  //{ path: '**', component: HomeComponent },// without changing the url
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactUsComponent,
    ProductDetailsComponent,
    OverviewComponent,
    SpecsComponent,
    SliderComponent,
    TouchSpinComponent,
    ViewChildrenComponent,
    CustomMaxDirective,
    CustomMinDirective,
    CacheComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes), //, {useHash: true}
    FormsModule,
    HttpClientModule
  ],
  providers: [BgSliderService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
